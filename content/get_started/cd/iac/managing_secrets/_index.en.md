---
title: "Managing secrets"
date: 2020-08-25T13:35:51+02:00
lastmod: 2020-08-27
weight: 3
draft: false
# search related keywords
keywords: ["secrets", "password", "safes", "keys"]
---


### Security problem

If you are deploying application, at some point you have to:  
  * rebuild infrastructure
  * change password
  * share credentials
  * revoke access

In any of those steps, there is a need to expose sensitive information to your deployment, so it can authenticate and access resources properly. We usually call those sensitive pieces of data a secret.  
The secret is any non-public information you want to control and limit access to.  

Most common used secrets when managing infrastructure are:  
  * passwords / PIN
  * API keys / tokens
  * SSH credentials
  * certificates  

How can we protect those secret information, but at the same time make them available to a service or person with proper authorization?

### Security control proposal

In complex environment, solution is to use secret management software accessible via network. Those solutions usually has multiple advantages:    

  * one place for secrets, hence projects are scalable
  * ability to revoke access
  * auditable activities
  * security - data are usually encrypted at rest  

Most common on-premise secret management solutions are:  

  * [Vault](https://www.vaultproject.io/) by HashiCorp
  * [Keywhiz](https://square.github.io/keywhiz/) by Square
  * [Confidant](https://github.com/lyft/confidant) by Lift
  * [Conjur](https://www.conjur.org/) by CyberArk

Of course, for simple projects and small deployments, it's possible to encrypt secret information in a _git_ repository, however, this solution does not scale and is not future-proof, so we consider it out-of-scope.
