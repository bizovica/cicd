---
title: "Technical documentation"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["documentation", "docs", "docu", "technical docs"]
---

### Security problem

Up to date technical documentation might not be the obvious candidate for a security topic, but it is.
Good documentation will contain up-to-date diagrams and application designs. 
Furthermore, it will warn users about potential pitfalls when using product or integrating with it.

Maintaining up to date technical documentation is cumbersome task and not very popular among developers.
For this reason documentation is usually either missing or is not updated frequently.

### Security control proposal

Having documentation in the same repository as the code, makes it easier for developers to update the documentation files.
Moreover, using VCS (version control system) makes tracking changes easier.
When content is automatically built and rendered using CI/CD pipelines, it makes publishing and updating documentation easier.
Anyone can review, update or propose changes to the repository. 
And having it written in text based format, such as markdown, makes it easy to browse even in the code editors.

### Reference implementation

#### Hugo

[Hugo](https://gohugo.io/) is a static site generator. 
It is used to generate static website from markdown files. 
You can use [themes](https://themes.gohugo.io/) to customize your documentation. 
Hence, users would always find a familiar interface, when browsing documentation to your products.

To get started with `Hugo` see its [Quick start guide](https://gohugo.io/getting-started/quick-start/).

In case your project is hosted on Gitlab, you can use the [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).
The pipeline job that deploys contents of your documentation website would look something like this:
```yaml
image: registry.gitlab.com/pages/hugo:0.73.0

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

### Practical implementation

#### Hugo

The [source code of this website](https://gitlab.com/magenta-cloud/cicd/) can serve as an example implementation of documentation site being served using `Hugo` and `Gitlab Pages`.
Website domain is configurable and Gitlab will even handle TLS certificates for you.

Also many products within Pan-net use `Hugo` for documentation rendering. 
Usually one of the jobs in `.gitlab-ci.yml` is dedicated to building the documentation.
