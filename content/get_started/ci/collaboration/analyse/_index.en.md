---
title: "Analyse"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["analyse", "analyze"]
---

### Security problem

Security requirements are often overlooked during analysis phase.
This can lead to wrong decisions later during design and/or development phases.
If security is not part of a mental context in which the analysis is done, your application or system might not pass the Privacy and Security Assessment (PSA) process.

### Security control proposal

During the analysis of business requirements, make sure you bear in mind security triad: Confidentiality, Integrity, and Availability.
Gather as much information about:
  - consumed/produced data  
  - the regulatory requirements  
  - locations that data will traverse  

Make sure you do the [cyberthreat modeling](https://en.wikipedia.org/wiki/Threat_model). This will aid your decisions when it comes to technologies used.
To get familiar with tools, cybersecurity best practices, security benchmarks, and current cybersecurity threats see [Center for Internet Security](https://www.cisecurity.org/) webpage.

Different cloud tools and technologies will have their own pages dedicated to security.
Make sure you are familiar with them before starting analysis, for example:

 - [Kubernetes security](https://kubernetes.io/docs/concepts/security/)
 - [Docker security](https://docs.docker.com/engine/security/security/)

Another good thing to be familiar with is what security tools and products are available by cloud providers. 
Knowing what is out there will help you with analysing problem at hand.
Here are links to some of the cloud providers security related infromation:
 - [Amazon Web Services](https://aws.amazon.com/security/)
 - [Google Cloud Platform](https://cloud.google.com/security/)
 - [Microsoft Azure](https://docs.microsoft.com/en-us/azure/security/azure-security)

