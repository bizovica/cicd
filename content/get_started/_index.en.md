---
title: "Get Started"
date: 2020-07-03T11:02:05+06:00
icon: "ti-arrow-circle-right"
description: "Start reading about CI/CD security topics"
type : "docs"
keywords: ["security", "controls", "CI/CD", "DevOps"]
---

This documentation will help you get started with securing your CI/CD workflow.

We strongly encourage you to have a look at Indrocution in a first place, where CI/CD security dilemmas and their solutions are described in a comprehensive and compact way.

The CI/CD security lifecysle is then reflected in the respective sections on the left side. I will let you understand in depth how we treat security dilemmas, what control mechanisms and which solutions do we propose.
