---
title: "Frequently Asked Questions"
draft: false
---

{{< faq "I'm not new to the Community, how can I cooperate?" >}}
First, you'll be added to our Magenta Cloud SLACK workspace - an easy and efficient way how to interact with all community members. We'll invite you also to our GitLab  [workspace](https://gitlab.com/magenta-cloud/cicd) where the actual solutions will be discussed and developed. You will have access to the TDAG's YAM [page](https://yam.telekom.de/groups/magenta-cloud-community) where we share our progress internally with our DTAG colleagues and in the same time we do have our public page [link](https://magenta-cloud.pan-net.cloud/) to share our work with broader community. Be welcome to Magenta Cloud CI/CD stream!
{{</ faq >}}

{{< faq "I want to contribute, how should I do that?" >}}
First, please visit our Introduction [page](https://magenta-cloud.pan-net.cloud/get_started/introduction/) to have an overview on the initiative itself.  After you're familiar with topics, please visit the How to Contribute [page](https://magenta-cloud.pan-net.cloud/contribute/). You can contribute either by implementing a solution, following the [General Flow](https://magenta-cloud.pan-net.cloud/contribute/general_flow/), by adding a [new topic](https://magenta-cloud.pan-net.cloud/contribute/adding_topics/) or [reviewing the code](https://magenta-cloud.pan-net.cloud/contribute/code_review/). As an observer, feel free to comment on user stories, feedback solutions and discuss the issues. In any time don't hesitate to Slack us in our Magenta Cloud workspace and we'll be happy to help you around! 
{{</ faq >}}

{{< faq "How does the organisation look like?" >}}
The complete list of current CI/CD Stream stakeholders and members can be viewed [here](https://yam.telekom.de/docs/DOC-698841). 
We're having a regular one-hour weekly Community call, every Friday at 9:00. On top of it, we're having ad-hoc technical sessions where we discuss current work, proposals and solutions on a hight expert level.
{{</ faq >}}

