---
title: "DT Group Security"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["security"]
---
Group Security Governance (GSG) is responsible for managing security-related issues for the Group strategically, consistently and sustainably, and is responsible throughout the Group for basic rules (policies/guidelines) on all aspects of security and monitors compliance with them. In addition, it is responsible for the area of public security and conducts investigations on behalf of and in the interests of the Group.
![DT group Security Governance and it's responsibility for issues regarding the security including various tasks.](group.png)
GSG's tasks also include:
- Development and transformation of the Group-wide security strategy.
- Protection of the Group's "crown jewels" and positioning of Deutsche Telekom in the protection of business interests against espionage and white-collar crime.
- Development, issuing and communicating of all binding Group policies on security issues and review of implementation and compliance.
- Establishment of transparency of the security situation in the Group, including through security controls and audits.
- Management and promotion of Group-wide cooperation on security.
- Conducting of investigations on behalf of and in the interests of the Group.
- Safeguarding management of the Group in the area of public safety and representation of the Group in these areas.
