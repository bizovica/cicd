---
title: "How to contribute"
weight: 2
icon: "ti-pencil"
description: "Contribution guide"
lastmod: 2020-07-08T08:41:42
type : "docs"
---

This document provides guidance on how to contribute to [CI/CD documentation project](https://gitlab.com/magenta-cloud/cicd).
Browse menu on the left for further information on specific area of contribution.

[Contribution flow](general_flow) - read more about workflow used by this documentation initiative.  
[Tips on adding topics](adding_topics) - general tips when adding topic content (What to write about and how to write).  
[Code review](code_review) - this section describes code review guidelines.  
