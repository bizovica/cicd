---
title: "Code review"
lastmod: 2020-07-08T08:41:42
weight: 3
draft: false
description: "Tips on doing code reviews"
# search related keywords
keywords: ["review"]
---

## General guidelines for everyone involved
   * Avoid [bikeshedding](https://exceptionnotfound.net/bikeshedding-the-daily-software-anti-pattern/)
   * Be polite and empathetic.
   * Be clear and stay on topic.

### Guidelines for contributors

   * Try to respond to questions/requests as fast as you can.


### Guidelines for reviewers
When doing code review, try to do following:  

   * Avoid rewriting the whole submission for review.
   * Coach person, whose writing your are reviewing, on best practices.
   * Don't let person wait too long on review.

