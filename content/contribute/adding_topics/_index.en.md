---
title: "Tips on adding topics"
lastmod: 2020-08-19T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["topics"]
---

### General considerations when writing

When adding or reviewing new topics make sure you:

1. Write a clear, short, and on point sentences.
2. Avoid too many cross reference links to other sites/resources.
3. Visually differentiate code snippets so user can copy/paste them.
4. Include all dependencies and prerequisites user might need for your example to work.
5. Test your code snippets on a vanilla system.

### How to choose a topic

You can choose topic to work on from our [issue backlog](https://gitlab.com/magenta-cloud/cicd/-/issues) or start your own based on areas described in [Introduction](../../get_started/introduction). It's best if you choose a topic that you are familiar with.

### Issues without a clear solution

If you have doubts about correctness of your solution, ask someone to review your proposal.
Gitlab is collaboration tool and should be used as such. 
Leverage the power of comments under issue to discuss the solution before diving into writing about topic you plan to add. 


