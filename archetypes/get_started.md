---
title: "{{ replace (strings.TrimLeft "_" .Name) "-" " " | title }}"
# date when content was last modified
lastmod: {{ .Date }}
# weight determines order of the posts in the side menu
weight: 1
draft: false
# search related keywords
keywords: ["keywords", "related", "to", "your", "topic"]
---


### Security problem

_Write here about security problem/dilema you are trying to address._

### Security control proposal

_Write here about possible solutions to a problem/dilema stated above._


### Reference implementation

_Add here a relevant implementation from reference documentation,
but make sure it's security compliant.
Sometimes reference documentation implementations are not secure,
with security being addressed in a footnote._

### Practical implementation

_Add here a practical implementation that is self contained.
Make sure you are not sharing any private or confidential information._
   

